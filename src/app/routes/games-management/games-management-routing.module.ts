import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GamesManagementComponent } from './games-management.component';

const routes: Routes = [
  {path: '', component: GamesManagementComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GamesManagementRoutingModule { }
