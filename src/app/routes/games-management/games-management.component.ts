import { Component, OnInit, TemplateRef } from '@angular/core';
import { DrawerHelper } from '@delon/theme';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalHelper } from '@delon/theme'; 
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { GamesManagementService } from 'src/app/services/games-management/games-management.service';
import { GamesDrawerComponent } from './games-drawer/games-drawer.component';
import { GamesModalComponent } from './games-modal/games-modal.component';
import {NzDrawerRef, NzDrawerService } from 'ng-zorro-antd/drawer';


interface Game {
  _id: string;
  name: string;
  logo: string;
  genre: string;
  status: string;
  playerType: string;
}

@Component({
  selector: 'app-games-management',
  templateUrl: './games-management.component.html',
  styleUrls: ['./games-management.component.css']
})
export class GamesManagementComponent implements OnInit {

  validateForm!: FormGroup;
  q: any = {
    _id: '',
    name: '',
    genre: '',
    status: '',
    type: '',
  };
  data: any[] = [];
  loading = false;
  status = [
    { index: 0, text: 'active', value: false, type: 'default', checked: false },
    { index: 1, text: 'inactive', value: false, type: 'success', checked: false },
  ];
  type = [
    { index: 0, text: 'active', value: false, type: 'default', checked: false },
    { index: 1, text: 'inactive', value: false, type: 'success', checked: false },
  ];
  genre = [
    { index: 0, text: 'active', value: false, type: 'default', checked: false },
    { index: 1, text: 'inactive', value: false, type: 'success', checked: false },
  ];
  description = '';
  totalCallNo = 0;
  expandForm = false;



  total = 1;
  listOfGames: Game[] = [];
  pageSize = 25;
  pageIndex = 1;
  selectedRow: any;

  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: Array<{ key: string; value: string[] }>
  ): void {
    this.loading = true;
    this.gameService.getGames(pageIndex, pageSize, sortField, sortOrder, filter).subscribe((data: any) => {
      this.loading = false;
      console.log('game data', data);
      this.total = data.body.totalNumberOfGames; // mock the total data here
      this.listOfGames = data.body.gameData;
    });
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    console.log(params);
    const { pageSize, pageIndex, sort, filter } = params;
    const currentSort = sort.find(item => item.value !== null);
    const sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
    this.loadDataFromServer(pageIndex, pageSize, sortField, sortOrder, filter);
  }

public rowClick(data: any): void{
console.log('data', data);
this.selectedRow = data._id;
this.drawerHelper.create('', GamesDrawerComponent, { record: data }).subscribe((res: any) => {
  console.log('back drawer response', res);
  this.msg.info(res);
});
}

public addGame(): void{
  this.modalHelper.open( GamesModalComponent, { data: { title: 'New Game', titleI18n: 'page-name' }}, 'md').subscribe((res: any) => {
    this.msg.info(res);
  });
}


  constructor(private gameService: GamesManagementService, public msg: NzMessageService, private fb: FormBuilder, private drawerHelper: DrawerHelper, private modalHelper: ModalHelper, private drawerService: NzDrawerService ) {}

  ngOnInit(): void {

    this.loadDataFromServer(this.pageIndex, this.pageSize, null, null, []);
  }


  getData(): void {
    console.log('filter data', this.q);
    if (this.q){
      const filter = this.q;
      for (var propName in filter) {
        if (
        filter[propName] === null ||
        filter[propName] === undefined ||
        filter[propName] === ''
        ) { delete filter[propName];
        } }
      let formObj = { filter };
      this.loading = true;
      this.gameService.filterGame(formObj).subscribe((data: any) => {
        this.loading = false;
        console.log('games filtered data', data);
        this.total = data.body.gameData.length; // mock the total data here
        console.log('filter data length', this.total);
        this.listOfGames = data.body.gameData;
      });
    } else{
      return;
    }
  }
}
