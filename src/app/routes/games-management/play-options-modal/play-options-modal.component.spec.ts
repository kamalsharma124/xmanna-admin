import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayOptionsModalComponent } from './play-options-modal.component';

describe('PlayOptionsModalComponent', () => {
  let component: PlayOptionsModalComponent;
  let fixture: ComponentFixture<PlayOptionsModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayOptionsModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayOptionsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
