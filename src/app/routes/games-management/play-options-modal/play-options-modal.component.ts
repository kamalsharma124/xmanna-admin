import { Component, OnInit, Input } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { Observable, Observer } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: "app-play-options-modal",
  templateUrl: "./play-options-modal.component.html",
  styleUrls: ["./play-options-modal.component.less"],
})
export class PlayOptionsModalComponent implements OnInit {
  form!: FormGroup;
  submitting = false;
  @Input() data: any;
  gameData: any;
  loading = false;
  avatarUrl?: string;
  
  genre: Array<{ value: string; label: string }> = [
    { value: 'skill', label: 'Skill' },
    { value: 'luck', label: 'luck' },
  ];
  developer: Array<{ value: string; label: string }> = [
    { value: 'dev1', label: 'Developer 1' },
    { value: 'dev2', label: 'Developer 2' },
  ];
  structure: Array<{ value: string; label: string }> = [
    { value: 'xiao', label: 'Struct 1' },
    { value: 'mao', label: 'Struct 2' },
  ];
  legalType: Array<{ value: string; label: string }> = [
    { value: 'xiao', label: 'Legal Type 1' },
    { value: 'mao', label: 'Legal Type 1' },
  ];
  ageRestriction: Array<{ value: string; label: string }> = [
    { value: 'xiao', label: '14+' },
    { value: 'mao', label: '18+' },
  ];
  owner: Array<{ value: string; label: string }> = [
    { value: 'xiao', label: 'Owner 1' },
    { value: 'mao', label: 'Owner 2' },
  ];

  submit(): void {
    for (const i in this.form.controls) {
      this.form.controls[i].markAsDirty();
      this.form.controls[i].updateValueAndValidity();
    }
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() =>
      this.form.controls.checkPassword.updateValueAndValidity()
    );
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.form.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  }

  beforeUpload = (file: NzUploadFile, _fileList: NzUploadFile[]) => {
    return new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
      if (!isJpgOrPng) {
        this.msg.error('You can only upload JPG file!');
        observer.complete();
        return;
      }
      const isLt2M = file.size! / 1024 / 1024 < 2;
      if (!isLt2M) {
        this.msg.error('Image must smaller than 2MB!');
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng && isLt2M);
      observer.complete();
    });
  }
  private getBase64(img: File, callback: (img: string) => void): void {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result!.toString()));
    reader.readAsDataURL(img);
  }

  handleChange(info: { file: NzUploadFile }): void {
    switch (info.file.status) {
      case 'uploading':
        this.loading = true;
        break;
      case 'done':
        // Get this url from response in real world.
        this.getBase64(info.file!.originFileObj!, (img: string) => {
          this.loading = false;
          this.avatarUrl = img;
        });
        break;
      case 'error':
        this.msg.error('Network error');
        this.loading = false;
        break;
    }
  }


  constructor(private fb: FormBuilder, private msg: NzMessageService) {
  }

  ngOnInit(): void {
    this.gameData = this.data.record;
    console.log('modal data', this.gameData);
    this.form = this.fb.group({
      challengeType: ['tvt', Validators.required],
      name: [null, [Validators.required]],
      positionIndex: [0, [Validators.required]],
      fee: [0, [Validators.required]],
      regOnly: [null ],
      players: [6, [Validators.required]],
      structure: [null, [Validators.required]],
      legalType: [null, [Validators.required]],
      restriction: [null, [Validators.required] ],
      description: [null, [Validators.required]],
      developer: [null, [Validators.required]],
      owner: [1, [Validators.min(1), Validators.max(3)]],
      publicUsers: [null, []],
    });
    if (this.gameData){
      this.formSetvalue();
    }
  }
  formSetvalue(): void{
    this.form.controls['name'].setValue(this.gameData.name);
    this.form.controls['genre'].setValue(this.gameData.genre);
    this.form.controls['description'].setValue(this.gameData.description);
    // this.form.controls['status'].setValue(this.gameData.status);
    // this.form.controls['playerType'].setValue(this.gameData.playerType);
    this.avatarUrl = this.gameData.logo;

  }
}
