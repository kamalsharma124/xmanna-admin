import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SinglePlayerComponent } from './single-player/single-player.component';

const routes: Routes = [
  {path: '', component: SinglePlayerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SinglePlayerRoutingModule { }
