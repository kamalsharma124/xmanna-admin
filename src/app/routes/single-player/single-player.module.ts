import { NgModule, Type } from '@angular/core';
import { SharedModule } from '@shared';
import { SinglePlayerRoutingModule } from './single-player-routing.module';
import { NzCollapseModule } from "ng-zorro-antd/collapse";
import { NzIconModule } from "ng-zorro-antd/icon";
import { IconModule } from '@ant-design/icons-angular';
import { NzImageModule } from "ng-zorro-antd/image";
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzPageHeaderModule } from "ng-zorro-antd/page-header";
import { CountDownModule } from "@delon/abc/count-down";
import { NzFormModule } from "ng-zorro-antd/form";
import { OnboardingModule } from "@delon/abc/onboarding";
import { G2BarModule } from "@delon/chart/bar";
import { G2CardModule } from "@delon/chart/card";
import { G2GaugeModule } from "@delon/chart/gauge";
import { G2MiniAreaModule } from "@delon/chart/mini-area";
import { G2MiniBarModule } from "@delon/chart/mini-bar";
import { G2MiniProgressModule } from "@delon/chart/mini-progress";
import { NumberInfoModule } from "@delon/chart/number-info";
import { G2PieModule } from "@delon/chart/pie";
import { G2RadarModule } from "@delon/chart/radar";
import { G2SingleBarModule } from "@delon/chart/single-bar";
import { G2TagCloudModule } from "@delon/chart/tag-cloud";
import { G2TimelineModule } from "@delon/chart/timeline";
import { QuickMenuModule } from "@delon/abc/quick-menu";
import { TrendModule } from "@delon/chart/trend";
import { G2WaterWaveModule } from "@delon/chart/water-wave";
import { CountdownModule } from "ngx-countdown";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NzFormTooltipIcon } from "ng-zorro-antd/form/ng-zorro-antd-form";
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { SinglePlayerComponent } from './single-player/single-player.component';
import { SinglePlayerDrawerComponent } from './single-player-drawer/single-player-drawer.component';

const COMPONENTS: Type<void>[] = [];

@NgModule({
  imports: [
    SharedModule,
    SinglePlayerRoutingModule,
    FormsModule,
    NzFormModule,
    ReactiveFormsModule,
    NzIconModule,
    NzImageModule,
    NzCollapseModule,
    NzPageHeaderModule,
    NzIconModule,
    IconModule,
    CountDownModule,
    CountdownModule,
    G2BarModule,
    G2CardModule,
    G2GaugeModule,
    G2MiniAreaModule,
    G2MiniBarModule,
    G2MiniProgressModule,
    G2PieModule,
    G2RadarModule,
    G2SingleBarModule,
    G2TagCloudModule,
    G2TimelineModule,
    G2WaterWaveModule,
    NumberInfoModule,
    TrendModule,
    QuickMenuModule,
    OnboardingModule,
    NzCollapseModule,
    NzTimelineModule,
    NzTypographyModule,
    NzMenuModule
  ],
  declarations: [COMPONENTS, SinglePlayerComponent, SinglePlayerDrawerComponent],
})
export class SinglePlayerModule { }
