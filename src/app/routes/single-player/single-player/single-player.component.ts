import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DrawerHelper } from '@delon/theme';
import { ModalHelper } from '@delon/theme'; 
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { SinglePlayerService } from 'src/app/services/single-player/single-player.service';
import { SinglePlayerDrawerComponent } from '../single-player-drawer/single-player-drawer.component';


interface Challenge {
  _id: string;
  name: string;
  logo: string;
  game: string;
  status: string;
  createdAt: string;
}

@Component({
  selector: 'app-single-player',
  templateUrl: './single-player.component.html',
  styleUrls: ['./single-player.component.less']
})
export class SinglePlayerComponent implements OnInit {

  validateForm!: FormGroup;
  q: any = {
    _id: '',
    name: '',
    genre: '',
    status: '',
    type: '',
  };
  data: any[] = [];
  loading = false;
  status = [
    { index: 0, text: 'active', value: false, type: 'default', checked: false },
    { index: 1, text: 'inactive', value: false, type: 'success', checked: false },
  ];
  type = [
    { index: 0, text: 'active', value: false, type: 'default', checked: false },
    { index: 1, text: 'inactive', value: false, type: 'success', checked: false },
  ];
  genre = [
    { index: 0, text: 'active', value: false, type: 'default', checked: false },
    { index: 1, text: 'inactive', value: false, type: 'success', checked: false },
  ];
  description = '';
  totalCallNo = 0;
  expandForm = false;



  total = 1;
  listOfChallenges: Challenge[] = [];
  pageSize = 30;
  pageIndex = 1;
  selectedRow: any;

  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: Array<{ key: string; value: string[] }>
  ): void {
    this.loading = true;
    this.challengeService.challengeAllList(pageIndex, pageSize, sortField, sortOrder, filter).subscribe((data: any) => {
      this.loading = false;
      console.log('challenges data', data);
      this.total = data.body.totalNumberOfChallenges; // mock the total data here
      this.listOfChallenges = data.body.challenges;
    });
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    console.log(params);
    const { pageSize, pageIndex, sort, filter } = params;
    const currentSort = sort.find(item => item.value !== null);
    const sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
    this.loadDataFromServer(pageIndex, pageSize, sortField, sortOrder, filter);
  }

public rowClick(data: any): void{
console.log('data', data);
this.selectedRow = data._id;
this.drawerHelper.create('', SinglePlayerDrawerComponent, { record: data }).subscribe((res) => {
  console.log('back drawer response', res);
  this.msg.info(res);
});
}

// public addGame(): void{
//   this.modalHelper.open( GamesModalComponent, { data: { title: 'New Game', titleI18n: 'page-name' }}, 'md').subscribe((res: any) => {
//     this.msg.info(res);
//   });
// }


  constructor(private challengeService: SinglePlayerService, public msg: NzMessageService, private fb: FormBuilder, private drawerHelper: DrawerHelper, private modalHelper: ModalHelper, ) {}

  ngOnInit(): void {

    this.loadDataFromServer(this.pageIndex, this.pageSize, null, null, []);
  }


  getData(): void {
    console.log('filter data', this.q);
    if (this.q){
      const filter = this.q;
      for (var propName in filter) {
        if (
        filter[propName] === null ||
        filter[propName] === undefined ||
        filter[propName] === ''
        ) { delete filter[propName];
        } }
      let formObj = { filter };
      this.loading = true;
      this.challengeService.filterChallenge(formObj).subscribe((data: any) => {
        this.loading = false;
        console.log('challenge filtered data', data);
        this.total = data.body.challenges.length; // mock the total data here
        console.log('filter data length', this.total);
        this.listOfChallenges = data.body.challenges;
      });
    } else{
      return;
    }
  }
}
