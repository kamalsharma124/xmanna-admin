import { Component, OnInit, ViewChild } from "@angular/core";
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
  NzTableSortOrder,
  NzTableQueryParams,
} from "ng-zorro-antd/table";
import { RandomUserService } from "src/app/services/mock-service/randomUser.service";
import { STChange, STColumn, STComponent, STData } from "@delon/abc/st";
import { _HttpClient } from "@delon/theme";
import { NzSafeAny } from "ng-zorro-antd/core/types";
import { NzMessageService } from "ng-zorro-antd/message";
import { NzModalService } from "ng-zorro-antd/modal";
import { map, tap } from "rxjs/operators";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { NzDrawerService } from "ng-zorro-antd/drawer";
import { UserDrawerComponent } from "./user-drawer/user-drawer.component";
import { DrawerHelper } from "@delon/theme";
import { NzIconModule } from "ng-zorro-antd/icon";

interface RandomUser {
  email: string;
  username: string;
  securityStatus: string;
  lastActive: string;
  fName: string;
  lName: string;
}

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css"],
})
export class UserComponent implements OnInit {
  validateForm!: FormGroup;
  paginationToggle: boolean = true;
  q: any = {
    username: "",
    email: "",
    loginType: "",
    lastActive: "",
    securityStatus: "",
    status: "",
    radioValue: ''
  };
  data: any[] = [];
  loading = false;
  securityStatus = [
    { index: 0, text: "fair", value: false, type: "default", checked: false },
    {
      index: 1,
      text: "warning",
      value: false,
      type: "default",
      checked: false,
    },
    { index: 2, text: "locked", value: false, type: "default", checked: false },
    {
      index: 3,
      text: "blacklisted",
      value: false,
      type: "default",
      checked: false,
    },
  ];
  status = [
    { index: 0, text: "active", value: false, type: "default", checked: false },
    {
      index: 1,
      text: "inactive",
      value: false,
      type: "success",
      checked: false,
    },
  ];
  description = "";
  totalCallNo = 0;
  expandForm = false;

  total = 1;
  listOfRandomUser: RandomUser[] = [];
  pageSize = 30;
  pageIndex = 1;

  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: Array<{ key: string; value: string[] }>
  ): void {
    this.loading = true;
    this.randomUserService
      .getUsers(pageIndex, pageSize, sortField, sortOrder, filter)
      .subscribe((data: any) => {
        this.loading = false;
        this.paginationToggle = true;
        console.log("user data", data);
        this.total = data.body.totalNumberOfUsers; // mock the total data here
        this.listOfRandomUser = data.body.userData;
      });
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    console.log(params);
    const { pageSize, pageIndex, sort, filter } = params;
    const currentSort = sort.find((item) => item.value !== null);
    const sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
    this.loadDataFromServer(pageIndex, pageSize, sortField, sortOrder, filter);
  }

  public rowClick(data: any): void{
    console.log('data', data);
    this.modalHelper
      .create('', UserDrawerComponent, { record: data })
      .subscribe((res) => {
        this.msg.info(res);
      });
  }

  constructor(
    private randomUserService: RandomUserService,
    private http: _HttpClient,
    public msg: NzMessageService,
    private fb: FormBuilder,
    private drawerService: NzDrawerService,
    private modalHelper: DrawerHelper
  ) {}

  ngOnInit(): void {
    this.loadDataFromServer(this.pageIndex, this.pageSize, null, null, []);
  }

  public reload(): void{
    this.loadDataFromServer(this.pageIndex, this.pageSize, null, null, []);
  }

  getData(): void {
    console.log("filter data", this.q);
    if (this.q) {
      const filter = this.q;
      for (var propName in filter) {
        if (
          filter[propName] === null ||
          filter[propName] === undefined ||
          filter[propName] === ""
        ) {
          delete filter[propName];
        }
      }
      let formObj = { filter };
      this.loading = true;
      this.randomUserService.filterUser(formObj).subscribe((data: any) => {
        this.loading = false;
        console.log("user data", data);
        this.paginationToggle = false;
        this.total = data.body.length; // mock the total data here
        console.log("filter data length", this.total);
        this.listOfRandomUser = data.body;
      }, (error) => {
        this.loading = false;
        console.log(error);
      });
    } else {
      return;
    }
  }
}
