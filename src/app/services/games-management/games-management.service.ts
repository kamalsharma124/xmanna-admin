import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

interface Game {
  _id: string;
  name: string;
  logo: string;
  genre: string;
  status: string;
  playerType: string;
}

@Injectable({ providedIn: 'root' })
export class GamesManagementService {
  base_url = 'https://dev.la-game.xmanna.com/'

  getGames(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filters: Array<{ key: string; value: string[] }>
  ): Observable<{ results: Game[] }> {
    let params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('results', `${pageSize}`)
      .append('sortField', `${sortField}`)
      .append('sortOrder', `${sortOrder}`);
    filters.forEach(filter => {
      filter.value.forEach(value => {
        params = params.append(filter.key, value);
      });
    });
    return this.http.post<{ results: Game[] }>(`${this.base_url + 'gamesAdmin/all'}`, {page: pageIndex, resPerPage: pageSize});
  }

  public filterGame(filterObj: any): Observable<{ results: Game[] }>{
    return this.http.post<{ results: Game[] }>(`${this.base_url + 'gamesAdmin/all'}`, filterObj);

  }

  constructor(private http: HttpClient) {}
}