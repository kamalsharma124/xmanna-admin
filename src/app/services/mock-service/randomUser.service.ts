import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { Observable } from 'rxjs';

interface RandomUser {
  email: string;
  username: string;
  securityStatus: string;
  lastActive: string;
  fName: string;
  lName: string;
}

@Injectable({ providedIn: 'root' })
export class RandomUserService {
  randomUserUrl = 'https://api.randomuser.me/';
  base_url = 'https://dev.la-user.xmanna.com/'

  getUsers(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filters: Array<{ key: string; value: string[] }>
  ): Observable<{ results: RandomUser[] }> {
    let params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('results', `${pageSize}`)
      .append('sortField', `${sortField}`)
      .append('sortOrder', `${sortOrder}`);
    filters.forEach(filter => {
      filter.value.forEach(value => {
        params = params.append(filter.key, value);
      });
    });
    return this.http.post<{ results: RandomUser[] }>(`${this.base_url+'usersAdmin/all'}`, {page: pageIndex, resPerPage:pageSize});
  }

  public filterUser(filterObj: any): Observable<{ results: RandomUser[] }>{
    return this.http.post<{ results: RandomUser[] }>(`${this.base_url+'usersAdmin/findUsers'}`, filterObj);

  }

  constructor(private http: HttpClient) {}
}