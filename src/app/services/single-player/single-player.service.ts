import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

interface Challenge {
  _id: string;
  name: string;
  logo: string;
  game: string;
  status: string;
  createdAt: string;
}

@Injectable({ providedIn: 'root' })
export class SinglePlayerService {
  base_url = 'https://dev.la-challenge-mq.xmanna.com/';

  challengeAllList(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filters: Array<{ key: string; value: string[] }>
  ): Observable<{ results: Challenge[] }> {
    let params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('results', `${pageSize}`)
      .append('sortField', `${sortField}`)
      .append('sortOrder', `${sortOrder}`);
    filters.forEach(filter => {
      filter.value.forEach(value => {
        params = params.append(filter.key, value);
      });
    });
    return this.http.post<{ results: Challenge[] }>(`${this.base_url + 'challengeAdmin/all'}`, {page: pageIndex, resPerPage: pageSize});
  }

  public filterChallenge(filterObj: any): Observable<{ results: Challenge[] }>{
    return this.http.post<{ results: Challenge[] }>(`${this.base_url + 'challengeAdmin/filter'}`, filterObj);

  }

  public updateChallengeStatus(updatedObj: any): Observable<{ results: Challenge[] }>{
    return this.http.post<{ results: Challenge[] }>(`${this.base_url + 'challengeAdmin/update'}`, updatedObj);

  }

  constructor(private http: HttpClient) {}
}