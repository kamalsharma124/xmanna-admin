import { Environment } from '@delon/theme';

export const environment = {
  production: true,
  base_url : 'https://la-user.xmanna.com/',
  game_url : 'https://la-game.xmanna.com/',
  play_options: 'https://la-playoptions.xmanna.com/',
  challenge_mq: 'https://la-challenge-mq.xmanna.com/',
  la_support: 'https://la-support.xmanna.com/',
  deposit_promotions: 'https://la-ledger.xmanna.com/',
  location: 'https://sandbox.spendsdk.com/api/location/',
  countries: 'https://la-countries.xmanna.com/',
  multiplayer: 'https://la-multiplayer.xmanna.com/',
  multiplayer_api_key: 'Ww0VmEvaSH0E3aGzHGSCdqu_SWtNhEsG',
  solitaire_url: 'https://la-solitaire.xmanna.com/',
  challenge_mq_blockChain_url: 'https://xmanna-blockchain-api.herokuapp.com/admin/getTransactionDetails/',
  useHash: true,
  api: {
    baseUrl: './',
    refreshTokenEnabled: true,
    refreshTokenType: 'auth-refresh',
  },
} as Environment;
