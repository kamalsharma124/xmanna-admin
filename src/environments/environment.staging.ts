// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    base_url : 'https://stage.la-user.xmanna.com/',
    game_url : 'https://stage.la-game.xmanna.com/',
    play_options : 'https://stage.la-playoptions.xmanna.com/',
    challenge_mq: 'https://stage.la-challenge-mq.xmanna.com/',
    la_support: 'https://stage.la-support.xmanna.com/',
    deposit_promotions: 'https://stage.la-ledger.xmanna.com/',
    location: 'https://sandbox.spendsdk.com/api/location/',
    countries: 'https://stage.la-countries.xmanna.com/',
    multiplayer: 'https://stage.la-multiplayer.xmanna.com/',
    multiplayer_api_key: 'Ww0VmEvaSH0E3aGzHGSCdqu_SWtNhEsG',
    solitaire_url: 'https://stage.la-solitaire.xmanna.com/',
    challenge_mq_blockChain_url: 'https://xmanna-blockchain-api.herokuapp.com/admin/getTransactionDetails/',
    };
    