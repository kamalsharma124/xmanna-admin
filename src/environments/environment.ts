// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { DelonMockModule } from '@delon/mock';
import { Environment } from '@delon/theme';
import * as MOCKDATA from '../../_mock';

export const environment = {
  production: false,
  base_url : 'https://dev.la-user.xmanna.com/',
  game_url : 'https://dev.la-game.xmanna.com/',
  play_options : 'https://dev.la-playoptions.xmanna.com/',
  challenge_mq: 'https://dev.la-challenge-mq.xmanna.com/',
  la_support: 'https://dev.la-support.xmanna.com/',
  deposit_promotions: 'https://dev.la-ledger.xmanna.com/',
  location:'https://sandbox.spendsdk.com/api/location/',
  countries:'https://dev.la-countries.xmanna.com/',
  multiplayer:'https://dev.la-multiplayer.xmanna.com/',
  multiplayer_api_key: 'Ww0VmEvaSH0E3aGzHGSCdqu_SWtNhEsG',
  solitaire_url: 'https://dev.la-solitaire.xmanna.com/',
  challenge_mq_blockChain_url: 'https://xmanna-blockchain-api.herokuapp.com/admin/getTransactionDetails/',
  useHash: true,
  api: {
    baseUrl: './',
    refreshTokenEnabled: true,
    refreshTokenType: 'auth-refresh',
  },
  modules: [DelonMockModule.forRoot({ data: MOCKDATA })],
} as Environment;


/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
